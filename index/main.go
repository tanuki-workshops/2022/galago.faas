package main

import (
	"fmt"
	"syscall/js"
)

var message string

func Html(_ js.Value, args []js.Value) interface{} {
	page := fmt.Sprintf(`
		<h1>🌕 Hello World 🚀😃😃</h1>
		<h2>GoLang is amazing</h2>
		<h3>Wasm is fantastic</h3>
		<h3>%v</h3>
	`, message)

	return page
}

func main() {

	// https://stackoverflow.com/questions/67437284/how-to-throw-js-error-from-go-web-assembly
	go func() {
		js.Global().Call("startCb")
	}()

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}

// https://8080-orange-silverfish-67x91xes.ws-eu17.gitpod.io/functions/web/html/go.index/0.0.0
