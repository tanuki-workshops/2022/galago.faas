#!/bin/bash

url_api="http://localhost:8080"
function_name="hello"
data='{"name":"bob"}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -H "DEMO_TOKEN: hello_world" \
  -X POST "${url_api}/functions/call/${function_name}"
echo ""
